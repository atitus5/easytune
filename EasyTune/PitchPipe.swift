//
//  PitchPipe.swift
//  EasyTune
//
//  Created by Andrew Titus on 6/24/15.
//  Copyright (c) 2015 Drew Titus. All rights reserved.
//

class PitchPipe: AKInstrument {
    var frequency            = AKInstrumentProperty(value: 440.0, minimum: 20.0, maximum: 2000.0)
    var amplitude            = AKInstrumentProperty(value: 0.2, minimum: 0,  maximum: 1)
    var carrierMultiplier    = AKInstrumentProperty(value: 1,   minimum: 0,  maximum: 3)
    var modulatingMultiplier = AKInstrumentProperty(value: 1,   minimum: 0,  maximum: 3)
    var modulationIndex      = AKInstrumentProperty(value: 10,  minimum: 0,  maximum: 30)
    
    override init() {
        super.init()
        
        addProperty(frequency)
        addProperty(amplitude)
        addProperty(carrierMultiplier)
        addProperty(modulatingMultiplier)
        addProperty(modulationIndex)
        
        let oscillator = AKFMOscillator(
            waveform: AKTable.standardSineWave(),
            baseFrequency: frequency,
            carrierMultiplier: carrierMultiplier,
            modulatingMultiplier: modulatingMultiplier,
            modulationIndex: modulationIndex,
            amplitude: amplitude)
        
        setAudioOutput(oscillator)
    }
}
