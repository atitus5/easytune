//
//  ViewController.swift
//  EasyTune
//
//  Created by Andrew Titus on 6/23/15.
//  Copyright (c) 2015 Drew Titus. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet var notePicker: UIPickerView!
    @IBOutlet var toggleButton: UIButton!
    
    let baseFreqs: [Float] = [16.350, 17.325, 18.355, 19.450, 20.600, 21.825, 23.125, 24.500, 25.960, 27.500, 29.135, 30.870]
    let notes = [["C", "C#", "D", "E♭", "E", "F", "F#", "G", "G#", "A", "B♭", "B"], ["0", "1", "2", "3", "4", "5", "6"]]

    let pipe = PitchPipe()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        notePicker.delegate = self
        notePicker.dataSource = self
        
        AKOrchestra.addInstrument(pipe)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPitch() {
        let noteRow: Int = notePicker.selectedRowInComponent(0)
        let baseFreq: Float = baseFreqs[noteRow]
        
        let octaveRow: Int = notePicker.selectedRowInComponent(1)
        let octave: Float = Float(octaveRow)
        
        let freq = baseFreq * pow(2.0, octave)
        
        pipe.frequency.value = freq
        AKOrchestra.updateInstrument(pipe)
    }

    // Play instrument or pause it, and update button text accordingly
    @IBAction func buttonPress() {
        if toggleButton.titleLabel!.text == "Play" {
            toggleButton.setTitle("Pause", forState: UIControlState.Normal)
            setPitch()
            pipe.play()
        } else {
            toggleButton.setTitle("Play", forState: UIControlState.Normal)
            pipe.stop()
        }
    }
    
    //MARK: - Delegates and data sources
    
    //MARK: - Data Sources
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return notes.count
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return notes[component].count
    }
    
    //MARK: - Delegates
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return notes[component][row]
    }
}

